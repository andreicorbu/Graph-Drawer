#include "src/GraphViewer.h"
#include <QtWidgets/QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QPalette pal = a.palette();
    pal.setColor(QPalette::Window, Qt::black);
    a.setPalette(pal);
    a.setWindowIcon(QIcon("GraphViewer.ico"));
    GraphViewer w;
    w.show();
    return a.exec();
}
