# Graph Viewer

Graph Viewer is an application written in C++ for dealing with drawing of graphs.

## Installation
Make sure you have the [Qt Framework](https://www.qt.io/download) and [Visual Studio](https://visualstudio.microsoft.com/vs/whatsnew/) installed.\
Clone the repository and open the solution with Visual Studio.

## Usage
If you want to draw a new Node just right click on the window.\
If you want to connect two Nodes left click on the first Node, then left click on the second Node.\
If you want to reposition a Node press Mouse3 Button(Scroll Button) and move the mouse while holding the Scroll Button.\
For going in light mode just uncheck the Dark Mode checkbox.\
If you want an undirected graph, click on the Directed button.\
Also, the program saves the Adjacency Matrix every time a new Node or Edge is created.

## Implementation
For implementing this I used the C++ Qt framework and the math from [here](https://math.stackexchange.com/questions/1314006/drawing-an-arrow).


## Pictures
![Dark Theme, Directed Graph](https://i.imgur.com/dYzFKgA.png)

![Light Theme, Directed Graph](https://i.imgur.com/jsIRd1v.png)

![Dark Theme, Undirected Graph](https://i.imgur.com/SL4EDas.png)

![Light Theme, Undirected Graph](https://i.imgur.com/QdN8E5r.png)
![Reposition Nodes functionality](https://i.imgur.com/s0tHNkW.gif)

> **_NOTE:_**  It is my first GUI application written.
