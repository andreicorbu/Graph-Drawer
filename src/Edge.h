#pragma once
#include "Node.h"
class Edge {
public:
	Edge() = default;
	Edge(Node firstNode, Node secondNode);
	Node GetFirstNode();
	Node GetSecondNode();
	Node GetFirstNode() const;
	Node GetSecondNode() const;
	void SetNodeCoords(int value, const QPointF& coords);
	bool operator ==(const Edge& rhs) const;
private:
	Node m_firstNode, m_secondNode;
};

