#pragma once
#include "Edge.h"
#include <vector>
class Graph {
public:
	Graph() = default;
	void AddNode(const Node& node);
	void AddEdge(const Edge& edge);
	const int GetNumberNodes() const;
	std::vector<Node>& GetNodes();
	std::vector<Edge>& GetEdges();
	std::vector<Node> GetNodes() const;
	std::vector<Edge> GetEdges() const;
	void UpdateAdjacencyMatrix(bool isDirected);
	bool CheckIfEdgeExist(const Edge& edgeSearched) const;
private:
	std::vector<Node> m_nodes;
	std::vector<Edge> m_edges;
	std::vector<std::vector<int>> m_adjacencyMatrix;
	void SaveAdjacencyMatrix() const;
};

