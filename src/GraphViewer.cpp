#include "GraphViewer.h"
#include <QMouseEvent>
#include <qpainter.h>
#include <unordered_set>
#include <fstream>
#include <iostream>

GraphViewer::GraphViewer(QWidget* parent)
	: QMainWindow(parent) {
	ui.setupUi(this);
	ui.radioButton->setChecked(true);
	ui.checkBox->setChecked(true);
	m_isFirstNode = false;
}

GraphViewer::~GraphViewer() {
	delete selectedNode;
}

void GraphViewer::mousePressEvent(QMouseEvent* event) {
	if (event->button() == Qt::MiddleButton) {
		for (Node& node : m_graph.GetNodes()) {
			if (node - Node(event->pos()) < 2 * m_nodeRadius) {
				selectedNode = &node;
				isSelectedToPosition = true;
				initialCoords = node.GetCoordinates();
			}
		}
	}
}

void GraphViewer::mouseMoveEvent(QMouseEvent* event) {
	if (isSelectedToPosition == true) {
		for (Node& node : m_graph.GetNodes()) {
			if (node == *selectedNode) {
				node.SetCoordinates(event->pos());
				for (Edge& edge : m_graph.GetEdges()) {
					edge.SetNodeCoords(node.GetInfo(), event->pos());
				}
				update();
				return;
			}
		}
	}
}

void GraphViewer::mouseReleaseEvent(QMouseEvent* event) {
	if (event->button() == Qt::RightButton) {
		CreateNode(event);
	}
	else if (event->button() == Qt::LeftButton) {
		CreateEdge(event);
	}
	else if (event->button() == Qt::MiddleButton) {
		isSelectedToPosition = false;
		selectedNode = nullptr;
		initialCoords.setX(0);
		initialCoords.setY(0);
	}
	update();
}

void GraphViewer::paintEvent(QPaintEvent* p) {
	QPen pen = SetTheme();
	QPainter painter(this);
	painter.setPen(pen);
	DrawNodes(painter);
	if (ui.checkBox->isChecked() == true) {
		pen.setColor(Qt::white);
	}
	else {
		pen.setColor(Qt::black);
	}
	DrawEdges(painter, pen);
}


void GraphViewer::CreateNode(QMouseEvent* event) {
	Node node(event->position());
	node.SetInfo(m_graph.GetNumberNodes() + 1);
	if (m_graph.GetNodes().size() != 0) {
		for (const Node& node : m_graph.GetNodes()) {
			if (node - Node(event->pos()) < ((m_nodeRadius * 2) + 5))
				return;
		}
	}
	m_graph.AddNode(node);
	update();
	m_graph.UpdateAdjacencyMatrix(ui.radioButton->isChecked());
}

void GraphViewer::CreateEdge(QMouseEvent* event) {
	const std::vector<Node> nodes = m_graph.GetNodes();
	Node selected;
	bool ok = false;
	for (const Node& node : nodes) {
		QPointF coords = node.GetCoordinates();
		double dist = node - Node(event->pos());
		if (dist <= m_nodeRadius) {
			selected = node;
			ok = true;
			break;
		}
	}
	if (ok) {
		CreateEdge(selected);
	}
	else {
		m_isFirstNode = false;
	}
}

void GraphViewer::CreateEdge(const Node& selected) {
	if (!m_isFirstNode) {
		m_firstNode = selected;
		m_isFirstNode = true;
	}
	else {
		if (m_firstNode == selected) {
			return;
		}
		Edge edgeToAdd = Edge(m_firstNode, selected);
		if (m_graph.CheckIfEdgeExist(edgeToAdd) == false) {
			m_graph.AddEdge(Edge(m_firstNode, selected));
		}
		m_isFirstNode = false;
		update();
		m_graph.UpdateAdjacencyMatrix(ui.radioButton->isChecked());
	}
}


QPen GraphViewer::SetTheme() {
	QPen pen;
	if (ui.checkBox->isChecked() == true) {
		this->setPalette(Qt::black);
		ui.radioButton->setStyleSheet("color: white");
		pen.setColor(Qt::white);
	}
	else {
		this->setPalette(Qt::white);
		ui.radioButton->setStyleSheet("color: black");
		pen.setColor(Qt::black);
	}
	return pen;
}

void GraphViewer::DrawNodes(QPainter& painter) {
	const std::vector<Node> nodes = m_graph.GetNodes();
	for (const Node& node : nodes) {
		QPointF coords = node.GetCoordinates();
		QRect rectangular(coords.x() - m_nodeRadius, coords.y() - m_nodeRadius, 2 * m_nodeRadius, 2 * m_nodeRadius);
		painter.drawEllipse(rectangular);
		QString num = QString::number(node.GetInfo());
		painter.drawText(rectangular, Qt::AlignCenter, num);
	}
}

void GraphViewer::DrawEdges(QPainter& painter, const QPen& pen) {
	const std::vector<Edge> edges = m_graph.GetEdges();
	std::unordered_set<Edge, functor> uniqueEdges;
	painter.setPen(pen);
	for (const Edge& edge : edges) {
		if (uniqueEdges.insert(edge).second == true) {
			QPointF p1 = edge.GetFirstNode().GetCoordinates();
			QPointF p2 = edge.GetSecondNode().GetCoordinates();
			double distance = edge.GetFirstNode() - edge.GetSecondNode();
			QLineF lineFromP1ToP2(p1, p2);
			lineFromP1ToP2.setLength(distance - m_nodeRadius);
			lineFromP1ToP2.setP1(lineFromP1ToP2.p1() + (p2 - lineFromP1ToP2.p2()));
			painter.drawLine(lineFromP1ToP2);
			if (ui.radioButton->isChecked()) {
				double x3, y3, x4, y4;
				x3 = p2.x() + (21.0 / distance) * ((p1.x() - p2.x()) * cos(17.5 * PI / 180) + (p1.y() - p2.y()) * sin(17.5 * PI / 180));
				y3 = p2.y() + (21.0 / distance) * ((p1.y() - p2.y()) * cos(17.5 * PI / 180) - (p1.x() - p2.x()) * sin(17.5 * PI / 180));
				x4 = p2.x() + (21.0 / distance) * ((p1.x() - p2.x()) * cos(17.5 * PI / 180) - (p1.y() - p2.y()) * sin(17.5 * PI / 180));
				y4 = p2.y() + (21.0 / distance) * ((p1.y() - p2.y()) * cos(17.5 * PI / 180) + (p1.x() - p2.x()) * sin(17.5 * PI / 180));
				painter.drawLine(lineFromP1ToP2.p2(), QPointF(x3, y3));
				painter.drawLine(lineFromP1ToP2.p2(), QPointF(x4, y4));
			}
			painter.drawLine(lineFromP1ToP2);
			update();
		}
		else {
			return;
		}
	}
}


