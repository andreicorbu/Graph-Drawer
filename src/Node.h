#pragma once
#include <QPoint>
class Node {
public:
	Node(int info = -1);
	Node(QPointF coords, int info = -1);
	void SetInfo(int info);
	void SetCoordinates(const QPointF& coords);
	const int GetInfo() const;
	const QPointF GetCoordinates() const;
	bool operator==(const Node& rhs) const;
	double operator -(const Node& rhs) const;
private:
	int m_info;
	QPointF m_coords;
};

