#pragma once
#include "Graph.h"
#include <QtWidgets/QMainWindow>
#include "ui_GraphViewer.h"

struct functor {
	size_t operator()(const Edge& edge) const {
		return edge.GetFirstNode().GetInfo() ^ edge.GetSecondNode().GetInfo();
	}
};

class GraphViewer : public QMainWindow {
	Q_OBJECT

public:
	GraphViewer(QWidget* parent = nullptr);
	~GraphViewer();
	void mouseReleaseEvent(QMouseEvent* event) override;
	void paintEvent(QPaintEvent* p) override;
	void mousePressEvent(QMouseEvent* event) override;
	void mouseMoveEvent(QMouseEvent* event) override;

private:
	void CreateNode(QMouseEvent* event);
	void CreateEdge(QMouseEvent* event);
	void CreateEdge(const Node& selected);
	QPen SetTheme();
	void DrawNodes(QPainter& painter);
	void DrawEdges(QPainter& painter, const QPen& pen);

private:	
	Ui::GraphViewerClass ui;
	Graph m_graph;
	const int m_nodeRadius = 10;
	bool m_isFirstNode = false;
	Node m_firstNode;
	const double PI = 3.14159265;
	Node* selectedNode= nullptr;
	bool isSelectedToPosition = false;
	QPointF initialCoords;
};
