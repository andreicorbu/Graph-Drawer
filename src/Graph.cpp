#include "Graph.h"
#include <fstream>

void Graph::AddNode(const Node& node) {
	m_nodes.push_back(node);
}

void Graph::AddEdge(const Edge& edge) {
	m_edges.push_back(edge);
}

const int Graph::GetNumberNodes() const {
	return int(m_nodes.size());
}

std::vector<Node>& Graph::GetNodes() {
	return m_nodes;
}

std::vector<Edge>& Graph::GetEdges() {
	return m_edges;
}

std::vector<Node> Graph::GetNodes() const {
	return m_nodes;
}

std::vector<Edge> Graph::GetEdges() const {
	return m_edges;
}

void Graph::UpdateAdjacencyMatrix(bool isDirected) {
	std::vector<std::vector<int>> adjacencyMatrix(m_nodes.size(), std::vector<int>(m_nodes.size()));
	for (const Edge& edge : m_edges) {
		adjacencyMatrix[edge.GetFirstNode().GetInfo() - 1][edge.GetSecondNode().GetInfo() - 1] = 1;
		if (isDirected == false) {
			adjacencyMatrix[edge.GetSecondNode().GetInfo() - 1][edge.GetFirstNode().GetInfo() - 1] = 1;
		}
	}
	m_adjacencyMatrix = std::move(adjacencyMatrix);
	SaveAdjacencyMatrix();
}

void Graph::SaveAdjacencyMatrix() const {
	std::ofstream fout("adjacencymatrix.txt");
	if (!fout) {
		throw("eroare la scriere in fisier");
	}
	else {
		fout << m_nodes.size() << "\n";
		for (size_t i = 0; i < m_nodes.size(); ++i) {
			for (size_t j = 0; j < m_nodes.size(); ++j) {
				fout << m_adjacencyMatrix[i][j] << " ";
			}
			fout << "\n";
		}
		fout.close();
	}
}

bool Graph::CheckIfEdgeExist(const Edge& edgeSearched) const {
	for (const Edge& edge : m_edges) {
		if (edge == edgeSearched)
			return true;
	}
	return false;
}
