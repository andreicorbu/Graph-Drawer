#include "Node.h"

Node::Node(int info) : m_info(info) {}

Node::Node(QPointF coords, int info) : m_coords(coords), m_info(info) {}

void Node::SetInfo(int info) {
	m_info = info;
}

void Node::SetCoordinates(const QPointF& coords) {
	m_coords = coords;
}

const int Node::GetInfo() const {
	return m_info;
}

const QPointF Node::GetCoordinates() const {
	return m_coords;
}

bool Node::operator==(const Node& rhs) const {
	if (m_coords == rhs.m_coords) {
		return true;
	}
	return false;
}

double Node::operator-(const Node& rhs) const {
	return sqrt(
		((m_coords.x() - rhs.m_coords.x()) * (m_coords.x() - rhs.m_coords.x())) + ((m_coords.y() - rhs.m_coords.y()) * (m_coords.y() - rhs.m_coords.y()))
	);
}



