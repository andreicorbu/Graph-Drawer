#include "Edge.h"

Edge::Edge(Node firstNode, Node secondNode) : m_firstNode(firstNode), m_secondNode(secondNode) {}

Node Edge::GetFirstNode() {
	return m_firstNode;
}

Node Edge::GetSecondNode() {
	return m_secondNode;
}

Node Edge::GetFirstNode() const {
	return m_firstNode;
}

Node Edge::GetSecondNode() const {
	return m_secondNode;
}

void Edge::SetNodeCoords(int value, const QPointF& coords) {
	if (m_firstNode.GetInfo() == value) {
		m_firstNode.SetCoordinates(coords);
	}
	else {
		if (m_secondNode.GetInfo() == value) {
			m_secondNode.SetCoordinates(coords);
		}
	}
}

bool Edge::operator==(const Edge& rhs) const {
	if (m_firstNode.GetCoordinates() == rhs.GetFirstNode().GetCoordinates() && m_secondNode.GetCoordinates() == rhs.m_secondNode.GetCoordinates())
		return true;
	return false;
}
